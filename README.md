## Manpages for Puppy Linux terminal.


### installation
IMPORTANT : first you need install "w3m" with ppm (Puppy Package Manager)

- curl https://framagit.org/noje77/terminal_manpages_for_puppy_linux/-/raw/main/mansh.sh >mansh
- bash mansh --install (follow steps)

### usage
Type mansh with manpage you want (exemple below for ls command) : 

$ mansh ls 

mansh go see inside Ubuntu Focal index made of manpages, if they don't found one , the bash script go look inside /usr/man/man1/.

### command options 
--help (see this help, mansh command without any name and argument do the same)

--install (this option launch mansh  installation and get manpages index)

--get (get manpages index,  if you remove them or manually install mansh, this is necessary to mansh to work)

--update (update this script serach where is mansh from $PATH, or tell you how to do)

--see (see index saved in /root/.manpage.index, helps you find which manpages are online)

--local (see local manpages on /usr/shar/man/man1, help you find manpages installed on your machine)

--clean (remove /root/. manpage. index, you need to get a new one after or mansh is broken)

### correct manpage 
In some case you go have wrong manpage, for correct this you need to go look inside index or local /usr/share/man/man1. 

For this you can use options “--see” or “-- local”, type “/“  then type the keyword, then validate to perform a search.


Then when you find the right command or software you want to see the manpage, type the full name with dot and number, just remove the ending .gz.

exemple : you want see the manpage of zegrep.1.gz (just type : zegrep.1)

### Definitly change man from Puppy Linux with mansh
Inside .bashrc file "/root/.bashrc" add an alias : 
alias man="mansh" 

If in some case, you want to back to original puppy linux man command, you can just type man with backslash before, (exemple below with ls manpage) : 

$ \man ls 

